import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

public class UrlMethods {

    public List<String> readAndConvertSentenceToList(String uri) {
        try {
            URL url = new URL(uri);
            URLConnection conn = url.openConnection();
            InputStream inputStream = conn.getInputStream();

            return new BufferedReader(new InputStreamReader(inputStream,
                    StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.toList());

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
