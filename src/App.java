import java.util.*;

public class App {

    public static void main(String[] args) {
        UrlMethods readFile = new UrlMethods();
        TextService textService = new TextService();
        String uri = "http://madbrains.github.io/java_course_test";

        List<String> words = textService.sentenceToWords(readFile.readAndConvertSentenceToList(uri));

        Map<String, Long> result = textService.countDuplicatesAndConvertToMap(words);

        result = textService.sortMap(result);

        textService.printWordCounts(result);
    }
}