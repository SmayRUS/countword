import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextService {

    public List<String> sentenceToWords (List<String> content) {
        List<String> words = new ArrayList<>();

        for (String sentence : content) {
            for (String s : sentence.split(" ")){
                if (!s.matches("[^a-zA-Zа-яёА-ЯЁ]"))
                    words.add(s.replaceAll("[^a-zA-Zа-яёА-ЯЁ+-]", "")
                            .toLowerCase());
            }
        }

        return words;
    }

    public Map<String, Long> countDuplicatesAndConvertToMap(List<String> inputList) {
        return new TreeMap<String, Long>(inputList
                //-----count duplicate-----
                .stream()
                .collect(Collectors.toMap(Function.identity(), v -> 1L, Long::sum)));
    }

    public Map<String, Long> sortMap(Map<String, Long> inputMap) {
        return inputMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (key, value) -> key, LinkedHashMap::new));
    }

    public void printWordCounts (Map<String, Long> inputMap) {
        inputMap.forEach((key, value) -> System.out.println(key + " - " + value + " раз"));
    }
}
